var connect = require('connect'),
bodyParser = require('body-parser'),
methodOverRide = require('method-override'),
cookieParser = require('cookie-parser'),
moment = require('moment'),
multer  = require('multer'),
path = require('path'),
routes = require('./routes'),
exphbs = require('express3-handlebars');

module.exports = function(app){
		
		app.engine('handlebars',exphbs.create({
			defaultLayout:'main',
			layoutsDir:app.get('views') + '/layouts',
			partialsDir:[app.get('views') + '/partials'],
			helpers: {
				timeago: function(timestamp) {
					return moment(timestamp).startOf('minute').fromNow();
				}
			}
		}).engine);
		
		app.set('view engine','handlebars');	
		
		app.use(require('morgan')('dev'));
		
		app.use(multer({ uploaddir: path.join(__dirname,'/public/upload/temp')}));
		
		app.use(methodOverRide());
		
		app.use(cookieParser('2990'));
		
		routes.initialize(app, new express.Router());
		
		app.use('/public/',express.static(path.join(__dirname, '../public')));
		
		if ('development' === app.get('env')) {
			app.use(require('errorhandler')());
		}
		
		routes.initialize(app,new express.Router());
		
		return app;
}