var fs = require('fs'),formidable = require('formidable'),http = require('http'),
util = require('util'),	md5 = require('md5'),Models = require('../models'), 
sidebar = require('../helpers/sidebar'), mv = require('mv');

module.exports = {
	index:function(req,res){
		//console.log("1.Called index");
		var viewModel = {
			image: {},
			comments:[]
			};
			Models.Image.findOne({ filename: { $regex: req.params.image_id } },
			function(err, image) {
					if (err) { throw err; }
					if (image) {
				//		console.log("2.Got Image");
						image.views = image.views + 1;
						viewModel.image = image;
						image.save();
						
						Models.Comment.find({ image_id: image._id}, {}, { sort: { 'timestamp':1 }},
							function(err, comments){
								if (err) { throw err; }
								viewModel.comments = comments;
								var vm = viewModel;
														
								sidebar(viewModel, function(a,viewModel) {	
									//console.log("3.Found comments -- " + util.inspect(viewModel));	
									res.render('image', viewModel);
								});
							}
						);						
					} else {
						res.redirect('/');
					}
			});
		
	},
	
	create:function(req,res){
	var count = 0;
		var saveImage = function(){	
		//console.log("count - " +  count);
		if (req.url == '/images' && req.method.toLowerCase() == 'post') {
			// parse a file upload
			var form = new formidable.IncomingForm();
			var file=req.files.file ;
			
			var possible = 'abcdefghijklmnopqrstuvwxyz0123456789',
			imgUrl = '';
			
			for(var i=0; i < 6; i+=1) {
				imgUrl += possible.charAt(Math.floor(Math.random() * possible.length));
			}
			Models.Image.find({ filename: imgUrl }, function(err, images) {
				if (images.length > 0) {
					saveImage();
				} else {
					var tempPath = file.path,
					ext = path.extname(file.name).toLowerCase(),
					targetPath = path.resolve('./public/upload/' + imgUrl + ext);
					
					if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif') {
						
						mv(tempPath, targetPath, function(err) {
							if (err) throw err;				//	console.log("Upload success");
							var newImg = new Models.Image({
								title: req.body.title,
								description: req.body.description,
								filename: imgUrl + ext
							});
							newImg.save(function(err, image) {
						//		console.log('Successfully inserted image: ' + image.filename);
								res.redirect('/images/' + image.uniqueId);
							});
						});
					} else {
						fs.unlink(tempPath, function () {
							if (err) throw err;
							console.log("Error");
							res.json(500, {error: 'Only image files are allowed.'});
						});
					}
				};		
			});	
		
	}
	}
	saveImage();
	},
	
	like: function(req, res) {
		Models.Image.findOne({ filename: { $regex: req.params.image_id } },
			function(err, image) {
				if (!err && image) {
					image.likes = image.likes + 1;
					image.save(function(err) {
						if (err) {
							res.json(err);
						} else {
							res.json({ likes: image.likes });
						}
					});
			}	
		});
	},
	
	comment: function(req, res) {
		Models.Image.findOne({ filename: { $regex: req.params.image_id } },
			function(err, image) {
				if (!err && image) {
					var newComment = new Models.Comment(req.body);
					newComment.gravatar = md5(newComment.email);
					newComment.image_id = image._id;
			//		console.log("new comment :" +util.inspect(req));
					newComment.save(function(err, comment) {
						if (err) { throw err; }
			//			console.log("comment :" +util.inspect(comment));
						res.redirect('/images/' + image.uniqueId + '#' + comment._id);
					});
				} else {
					res.redirect('/');
				}
		});
	},

	remove: function(req, res) {
		Models.Image.findOne({ filename: { $regex: req.params.image_id }
	},
	function(err, image) {
			if (err) { throw err; }
			fs.unlink(path.resolve('./public/upload/' + image.filename),
			function(err) {
				if (err) { throw err; }
				Models.Comment.remove({ image_id: image._id},
				function(err) {
					image.remove(function(err) {
						if (!err) {
							res.json(true);
						} else {
							res.json(false);
						}
					});
				});
			});
		});
	}
}