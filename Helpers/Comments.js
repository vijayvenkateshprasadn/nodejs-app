var models = require('../models'),async = require('async'),util = require('util');


module.exports = {
	newest: function(callback) {
		models.Comment.find({}, {}, { limit: 5, sort: { 'timestamp': -1 } },
			function(err, comments){
				var attachImage = function(comment, next) {
					models.Image.findOne({ _id : comment.image_id},
					function(err, image) {
						if (err) throw err;
						comment.image = image;
						next(err);
						//console.log("Comment Module each inside ");
					});
				};
				async.each(comments, attachImage,
				function(err) {
					if (err) throw err;
					//console.log("Comment Module each " + util.inspect(comments));
					callback(err, comments);
				});
			});
	}
};

	