express = require('express'),
config = require('./server/configure'),
mongoose = require('mongoose'),
app = express();

app.set('port',process.env.port||3300);
app.set('Views',__dirname+ '/Views');

app = config(app);

mongoose.connect('mongodb://localhost/imgPloadr');
mongoose.connection.on('open', function() {
	console.log('Mongoose connected.');
});


var server = app.listen(app.get('port'),function(){
	console.log('Server up: http://localhost:' + app.get('port'));
});